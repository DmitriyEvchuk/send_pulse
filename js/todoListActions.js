
const  ELEMENT_NODE = 1;
let tdActionGroup = document.querySelectorAll(".actionGroup");
let parrentForUpdate = null;
let bind = {"Time": "#newTime",
    "Head": "#newHead",
    "Action": "#newAction"
};

for (let i = tdActionGroup.length - 1; i >= 0; i--) {
    let td = tdActionGroup[i];
    td.addEventListener("click", clickOnTableElem);
}

function clickOnTableElem(event) {

    parrentForUpdate = event.target.parentNode;
    let children = event.target.parentNode.childNodes;

    for (let indx in children) {
        let child = children[indx];
        if (child.nodeType === ELEMENT_NODE) {
            document.querySelector(bind[child.className]).value
                    = child.innerText;
        }
    }

}

let defaultText = {"Time": "YYYY-MM-DD HH:MM:SS",
    "Head": "add header",
    "Action": "add action"
};

document.querySelector("#updateButton").addEventListener("click", function () {

    let values = [];
    if (parrentForUpdate != null) {

        for (let className in bind) {
            if (bind[className])
                textArea = document.querySelector(bind[className]);
            values[className] = textArea.value;
            textArea.value = defaultText[className];
        }

        if (validation(values)) {
            values["newtimeset"] = new Date().getTime();
            values["timeset"] = parrentForUpdate.id.split('/')[1];
            sendValue(values, "../../ajax/update") ? updateTODOTable(values) :
                    alert("Something wrong");
        }

    } else {

        alert('change element for update');
    }
});

document.querySelector("#addButton").addEventListener("click", function () {
    parrentForUpdate = null;
    let values = [];

    for (let className in bind) {
        if (bind[className])
            textArea = document.querySelector(bind[className]);
        values[className] = textArea.value;
        textArea.value = defaultText[className];
    }

    if (validation(values)) {
        values["newtimeset"] = 0;
        values["timeset"] = new Date().getTime();
        sendValue(values, "../../ajax/addAction") ? insertElemInTable(createElement(values)) :
                alert("Something wrong maybe action with " + values["Time"] +
                        " time exist");
    }


});

document.querySelector("#removeButton").addEventListener("click", function () {
    values = [];
    values["timeset"] = parrentForUpdate.id.split('/')[1];

    sendValue(values, "../../ajax/removeAction") ? parrentForUpdate.remove() :
            alert("Something wrong");

    for (let className in bind) {
        if (bind[className])
            textArea = document.querySelector(bind[className]);
        textArea.value = defaultText[className];
    }

    parrentForUpdate = null;
});

className = ["Time", "Head", "Action"];
styles = ["20%", "30%", "50%"];

function createElement(values) {

    let tr = document.createElement("tr");
    tr.id = new Date(values['Time']).getTime() + "/" + values["timeset"];
    tr.className = "actionGroup";

    for (let i = 0; i < 3; i++) {
        let td = document.createElement("td");

        td.className = className[i];
        td.textContent = values[className[i]];
        td.style.width = styles[i];
        tr.appendChild(td);
    }
    return tr;
}

function updateTODOTable(values) {

    updateValue = parrentForUpdate.cloneNode(true);
    parrentForUpdate.remove();
    parrentForUpdate = null;

    let children = updateValue.childNodes;

    for (let indx in children) {
        let child = children[indx];
        if (child.nodeType === ELEMENT_NODE) {
            child.innerText = values[child.className];
        }
    }
    updateValue.id = new Date(values['Time']).getTime() + "/" + values["newtimeset"];
    insertElemInTable(updateValue);
}

function insertElemInTable(elem) {
    elem.addEventListener("click", clickOnTableElem);
    let tdActionGroup = document.body.getElementsByClassName("actionGroup");
    newActionTime = elem.id.split('/')[0];
    let td;

    if (tdActionGroup.length == 0)
        document.querySelector(".todo-table").appendChild(elem);

    for (let i = 0; i < tdActionGroup.length; i++) {
        td = tdActionGroup[i];
        actionTime = td.id.split('/')[0];

        if (actionTime > newActionTime ) {
            document.querySelector(".todo-table").insertBefore(elem, td);
            break;
        } else {
            document.querySelector(".todo-table").appendChild(elem);

        }
    }
}

function sendValue(values, url) {
    var req = new XMLHttpRequest();
    req.open("post", url, false);

    var body = 'time=' + encodeURIComponent(values["Time"]) +
            '&header=' + encodeURIComponent(values["Head"]) +
            '&acion=' + encodeURIComponent(values["Action"]) +
            '&timeset=' + encodeURIComponent(values["timeset"]) +
            '&newtimeset=' + encodeURIComponent(values["newtimeset"]);

    req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    req.send(body);

    //console.log(req.getResponseHeader("Insert"));
    //console.log(req.responseText);
    if (req.getResponseHeader("Update") == 'true' || req.getResponseHeader("Insert") == 'true'
            || req.getResponseHeader("Remove") == 'true') {
        return true;
    }
    return false;

}

function validation(values) {

    for (let key in values) {

        values[key] = values[key].trim();
        if (!values[key].length) {
            alert("Fields " + key + " can't be empty");
            return false;
        }
    }

    values['Time'] = values['Time'].trim();
    var dateTime = /\d{4}-\d{1,2}-\d{1,2} \d{1,2}:\d{2}:\d{2}/;

    if (!dateTime.test(values['Time'])) {
        alert("Date must be in format YYYY-MM-DD HH:MM:SS ");
        return false;
    }

    if (new Date().getTime() > new Date(values['Time']).getTime()) {
        alert("Date can not be less than current  ");
        return false;
    }
    return true;
}

