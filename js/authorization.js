
var form = document.querySelector("form");

function validatorSendValue() {
    var req = new XMLHttpRequest();
    req.open("post", "../ajax/authorization", false);
    var body = 'email=' + encodeURIComponent(form.elements["email"].value) +
            '&password=' + encodeURIComponent(form.elements["password"].value);

    req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    req.send(body);

    if (req.getResponseHeader("Authenticate") == 'true') {
        return true;
    }
    return false;

}

form.addEventListener("submit", function (event) {

    if (!validatorSendValue()) {
        event.preventDefault();
        alert("user doesn'texist")
    }

});