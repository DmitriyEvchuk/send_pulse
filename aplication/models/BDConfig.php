<?php

class BDConfig {

    private static $host = "localhost";
    private static $user = "root";
    private static $dbName = "todo_db";
    private static $password = "1111";

    

    static function getHost() {

        return self::$host;
    }

    static function getUser() {

        return self::$user;
    }
    
    static function getDBName() {

        return self::$dbName;
    }
    
    static function getPassword() {

        return self::$password;
    }

}
