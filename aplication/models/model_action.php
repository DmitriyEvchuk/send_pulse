<?php

include_once "BDConfig.php";

//source c:/createTodoDB.sql;
class Model_Actions {

    private $connect;

    function __construct() {


        $this->connect = mysqli_connect(BDConfig::getHost(), BDConfig::getUser()
                , BDConfig::getPassword(), BDConfig::getDBName());
    }

    public function getAllActionForUserId($id) {

        $actions = mysqli_query($this->connect, "SELECT * FROM todo_list where id_user={$id} ");
        $actions = mysqli_fetch_all($actions, MYSQLI_ASSOC);

        $toSendAction = [];
        foreach ($actions as $action) {

            $toSendAction[strtotime(date($action['time']))] = $action;
        }
        ksort($toSendAction);
        return $toSendAction;
    }

    public function updateAction($userID, $timset, $acion, $header, $date, $newTimset) {

        $this->escape($acion, $header, $date);

        $update = mysqli_query($this->connect, "UPDATE todo_list
SET header='$header',acion='$acion',time='$date',timset='$newTimset'
WHERE id_user='$userID' and timset='$timset'");

        return $update ? TRUE : FALSE;
    }

    private function escape(&$acion, &$header, &$date) {

        $acion = mysqli_real_escape_string($this->connect, $acion);
        $header = mysqli_real_escape_string($this->connect, $header);
        $date = mysqli_real_escape_string($this->connect, $date);
    }

    public function addAction($userID, $timset, $acion, $header, $date) {

        $this->escape($acion, $header, $date);

        if (!$userID || !$timset || !$acion || !$header || !$date)
            return FALSE;

        $add = mysqli_query($this->connect, "INSERT INTO todo_list 
            (id_user, header,acion,time, timset)
              VALUES ('$userID', '$header','$acion','$date',$timset);");

        return $add ? TRUE : FALSE;
    }

    public function removeAction($userID, $timset) {

        $remove = mysqli_query($this->connect, "delete from todo_list "
                . "where id_user=$userID and timset=$timset;");

        return $remove ? TRUE : FALSE;
    }

    public function __destruct() {
        mysqli_close($this->connect);
    }

}
