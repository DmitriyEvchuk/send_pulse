<?php

class RoutingConfig {

    private static $actionList = array("mainpage" => array("main", "registration","logout"),
        "ajax" => array("authorization", "registration", "update", "addaction",
            "removeaction"));
    private static $defultController = "mainpage";
    private static $defultAction = "main";

    static function actionExist($controller, $action) {
        
        return array_key_exists($controller, self::$actionList) &&
                in_array($action, self::$actionList[$controller]) ?
                TRUE : FALSE;
    }

    static function getDefaultAction() {

        return self::$defultAction;
    }

    static function getDefaultController() {

        return self::$defultController;
    }

}
