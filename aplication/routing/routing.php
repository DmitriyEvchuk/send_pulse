<?php

include_once 'routingConfig.php';

class Routing {

    static function execute() {

        $url = explode('/', $_SERVER['REQUEST_URI']);
        $controllerName = isset($url[1]) ? strtolower($url[1]) : RoutingConfig::getDefaultController();
        $action = isset($url[2]) ? strtolower($url[2]) : RoutingConfig::getDefaultAction();

        if (!RoutingConfig::actionExist($controllerName, $action)) {
            $controllerName = RoutingConfig::getDefaultController();
            $action = RoutingConfig::getDefaultAction();
        }

        $controllerName = 'Controller_' . $controllerName;
        $action = 'action_' . $action;
        $controller = new $controllerName;

        if (method_exists($controller, $action)) {
            call_user_func(array($controller, $action), $url);
        } else {
            echo '404';
        }
    }

}
