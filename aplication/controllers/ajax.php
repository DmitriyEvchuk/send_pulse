<?php

include_once "aplication/models/model_users.php";

//YYYY-MM-DD HH:MM:SS
class Controller_Ajax {

    public $model;

    function __construct() {

        session_start();
    }

    function action_registration() {

        $this->model = new Model_Users();

        $email = $_POST['email'];
        $password = $_POST['password'];
        
        $registration = isset($email) && isset($password) ?
                $this->model->userInsert($email, $password) : FALSE;

        $registration ? header('Registration: true') : header('Registration: false');
    }

    function action_authorization() {

        $this->model = new Model_Users();

        $email = $_POST['email'];
        $password = $_POST['password'];

        $userID = isset($email) && isset($password) ?
                $this->model->userExist($email, $password) : FALSE;

        if ($userID) {

            $_SESSION[session_id()] = $userID;
            header('Authenticate: true');
            return TRUE;
        }

        header('Authenticate: false');
        return false;
    }

    function action_update() {

        $this->model = new Model_Actions();

        $userID = $_SESSION[session_id()];
        if ($userID) {
            $date = $_POST['time'];
            $header = $_POST['header'];
            $acion = $_POST['acion'];
            $timset = $_POST['timeset'];
            $newTimset = $_POST['newtimeset'];
        }
        $update = isset($date) && isset($header) && isset($acion) && isset($timset) ?
                $this->model->updateAction($userID, $timset
                        , $acion, $header, $date, $newTimset) : FALSE;


        $update ? header('Update: true') : header('Update: false');
    }

    function action_addAction() {

        $this->model = new Model_Actions();

        $userID = $_SESSION[session_id()];
        if ($userID) {
            $date = $_POST['time'];
            $header = $_POST['header'];
            $acion = $_POST['acion'];
            $timset = $_POST['timeset'];
        }
        $add = isset($date) && isset($header) && isset($acion) && isset($timset) ?
                $this->model->addAction($userID, $timset
                        , $acion, $header, $date) : FALSE;

        $add ? header('Insert: true') : header('Insert: false');
    }

    function action_removeAction() {

        $this->model = new Model_Actions();

        $userID = $_SESSION[session_id()];
        if ($userID) {
            $timeset = $_POST['timeset'];
        }
        $remove = isset($timeset) ?
                $this->model->removeAction($userID, $timeset) : FALSE;

        $remove ? header('Remove: true') : header('Remove: false');
    }

}
