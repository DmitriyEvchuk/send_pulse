<?php

//include_once "aplication/models/model_users.php";

class Controller_Main {

    public $model;
    public $view;

    function __construct() {
        $this->view = new View();
        session_start();
    }

    function action_main() {

        $this->view->getView("authorization.php", "main.php"
                , array("title" => "authorization"));
    }

    function action_registration() {
        $this->view->getView("registration.php", "main.php"
                , array("title" => "registration"));
    }

    function action_logout() {
        session_destroy();
        echo '<h1>GOOD BY</h1>';
    }

}
