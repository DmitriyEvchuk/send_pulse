<?php

include_once "main.php";
include_once "aplication/models/model_action.php";

class Controller_MainPage extends Controller_Main {

    function __construct() {
        parent::__construct();
    }

    function action_main() {

        $id_user = isset($_SESSION[session_id()]) ?
                $_SESSION[session_id()] : NULL;

        if (isset($id_user)) {

            $Model_Actions = new Model_Actions();
            $actions = $Model_Actions->getAllActionForUserId($id_user);

            $this->view->getView("todolist.php", "main.php", array("title" => "main"
                , "actions" => $actions));
        } else {
            parent::action_main();
        }
    }

}
