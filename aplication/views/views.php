<?php

class View {

    function getView($content, $template, $data = null) {

        if (is_array($data)) {

            extract($data, EXTR_OVERWRITE);
        }

        include $template;
    }

}
