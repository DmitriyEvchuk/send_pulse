<?php

require '..\..\aplication\models\model_users.php';

class UserActionDBTests extends PHPUnit_Framework_TestCase {

    private $model_users;

    protected function setUp() {
        $this->model_users = new Model_Users();
    }

    protected function tearDown() {
        $this->model_users = NULL;
    }

    public function testUserExist() {

        $result = $this->model_users->userExist("dmitriy.evchuk@gmail.com", "1111");
        $this->assertEquals(1, $result);

        $result = $this->model_users->userExist("dmitriy.evchuk@gmail.com", "11121");
        $this->assertFalse($result);

        $result = $this->model_users->userExist(null, null);
        $this->assertFalse($result);
    }

    public function testEmailExist() {

        $result = $this->model_users->emailExist("dmitriy.evchuk@gmail.com");
        $this->assertTrue($result);

        $result = $this->model_users->emailExist("inna.polgui@gmail.com");
        $this->assertFalse($result);

        $result = $this->model_users->emailExist(null);
        $this->assertFalse($result);

        $result = $this->model_users->emailExist(1111);
        $this->assertFalse($result);
    }

    public function testUserInsert() {
        
        $result = $this->model_users->userInsert("dmitriy.evchuk@gmail.com","qwerty");
        $this->assertFalse($result);
        
        $result = $this->model_users->userInsert(null,null);
        $this->assertFalse($result);
        
        $result = $this->model_users->userInsert("","");
        $this->assertFalse($result);
        
        $result = $this->model_users->userInsert("newdmitriy.evchuk@gmail.com","2222");
        $this->assertTrue($result);
        
    }

}
