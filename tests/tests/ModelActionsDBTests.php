<?php

require '..\..\aplication\models\model_action.php';

class ModelActionsDBTests extends PHPUnit_Framework_TestCase {

    private $model_actions;

    protected function setUp() {
        $this->model_actions = new Model_Actions();
    }

    protected function tearDown() {
        $this->model_actions = NULL;
    }

    public function testGetAllActionForUserId() {

        $expected = array(strtotime('2017-07-19,07:11:30') => array("id_user" => '1',
                "header" => "shoping", "acion" => "buy milk", "time" => "2017-07-19 07:11:30",
                "timset" => '150023378675', 'ID' => '1'),
            strtotime('2017-08-16 12:00:00') => array("id_user" => '1',
                "header" => "repair car", "acion" => "new brakes", "time" => "2017-08-16 12:00:00",
                "timset" => '150023380369', 'ID' => '2'),
            strtotime('2017-08-16 20:00:00') => array("id_user" => '1',
                "header" => "son", "acion" => "parent meeting", "time" => '2017-08-16 20:00:00',
                "timset" => '150023355574', 'ID' => '3'));

        $actual = $this->model_actions->getAllActionForUserId(1);

        $this->assertEquals($expected, $actual);
    }

    public function testUpdateActionFalse() {

        $rezult = $this->model_actions->updateAction(1
                , '150023355574', 'buy meet', 'shoping', '2017-08-16 50:00:00', "150023355590");

        $this->assertFalse($rezult);
    }

    public function testUpdateActionTrue() {

        $rez = $this->model_actions->updateAction(1
                , '150023355574', 'buy beer', 'shoping', '2017-08-16 20:00:00', "150023355590");

        $this->assertTrue($rez);
    }

    public function testAddAction() {

        $rez = $this->model_actions->addAction(1, '150023355591', 'buy milk', 'shoping'
                , '2017-08-16 21:00:00');

        $this->assertTrue($rez);

        $rez = $this->model_actions->addAction(1, '150023355590', 'buy milk', 'shoping'
                , '2017-08-16 81:00:00');

        $this->assertFalse($rez);

        $rez = $this->model_actions->addAction(1, '150023355590', null, 'shoping'
                , '2017-08-16 11:00:00');

        $this->assertFalse($rez);
    }

    public function testRemoveAction() {

        $rez = $this->model_actions->removeAction(1,"150023355591");

        $this->assertTrue($rez);
    }

}
