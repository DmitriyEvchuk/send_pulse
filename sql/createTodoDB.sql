CREATE DATABASE IF NOT EXISTS todo_db;
USE todo_db;
CREATE TABLE IF NOT EXISTS todo_list (
    ID int NOT NULL AUTO_INCREMENT,
    id_user SMALLINT NOT NULL,
    header TEXT NOT NULL,
    acion TEXT NOT NULL,
    time DATETIME NOT NULL,
    timset bigint NOT NULL,
    PRIMARY KEY (ID)
);

CREATE TABLE IF NOT EXISTS users (
    ID int NOT NULL AUTO_INCREMENT,
    email TEXT NOT NULL,
    password TEXT NOT NULL,
    PRIMARY KEY (ID)
);


INSERT INTO users (email, password)
VALUES ('dmitriy.evchuk@gmail.com', '1111');

INSERT INTO todo_list (id_user, header,acion,time, timset)
VALUES (1, 'shoping','buy milk','2017-07-19,07:11:30',150023378675),(1, 'repair car','new brakes','2017-08-16,12:00:00',150023380369),
(1, 'son','parent meeting','2017-08-16,20:00:00',150023355574);

